#include "initg.h"

int main(){
	uint8_t tcl = 0, resul = 0, opj = 0; //entrada do teclado
	Rbot * usu;
	Rbot * opon;

	screenSetUp(0);
	//posição inicial dos bots[x,y]. o curses recebe[y, x]
	opon = botSetUp(4, 4);
	usu = botSetUp(14, 15);

	while(!resul){
		if((*opon).pbang){
			finaliz(1);
			break;
		}
		else{
			mvwprintw(sWin, 1, 1, "Aguardando comando...");
			wrefresh(sWin);
			tcl = getch();
			wclear(sWin);

			if(tcl == 'q'){
				finaliz(1);
				break;
			}
			else if(tcl == 'j'){
				if((*usu).pbang){
					resul = ptBomb();
					if(resul){
						finaliz(2);
						break;
					}
					opJogada(opon,opj);
					opj++;
				}
				else{
					pJogada(usu);
					opJogada(opon,opj);
					opj++;
				}
			}
			//entrTcl(tcl, usu);
			refresh();
		}
	}
	//finaliz(1);
	//endwin(); //[USAR SEMPRE] -- Termina o curses para o funcionamento normal do terminal.
	return 0;
}

int8_t finaliz(int8_t cond){
	endwin();
	if (cond == 1){
		printf("\n\n\nNOOOOOOSSSSSSS\n PERDEU.....\nÉ ISSO!!!\n");
	}
	else if (cond != 1){
		printf("\n\n\nMUITO BEM....\n Ganhou \n Parabéns!!!!!\nTOOOOOOP\n Dale memo!\nNICE!\n BACANA!\n");
	}

	return 0;
}
