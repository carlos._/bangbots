#include"initg.h"


static void escSta(int8_t x, int8_t y, char ent, WINDOW *wind){
	if (ent != 'e'){
		mvwprintw(wind, y, x, "mov %c", ent);
	}
	else if (ent == 'e'){
		mvwprintw(wind, y, x, "exec ", ent);
	}
	wrefresh(wind);
}


void chkMov(int16_t nX, int16_t nY, Rbot *usu){
	//Retorna caracter que ocupa a posição futura do robô.
	switch(mvinch(nY,nX)){
		case 32:
			moverBot(nY, nX, usu);
			break;
		case '!':
			(*usu).pbang = true;
			break;
		default:
			move((*usu).ypos, (*usu).xpos);
			break;
	}
}


int16_t pJogada(Rbot * usu){
	uint8_t movts[5] = {'u', 'r', 'd', 'l', 'e'}, sta = ' ', \
					    nmovts[8] = {' '}, nsel = 0, i = 0;
	int16_t sel = 0, esco;

	keypad(sWin, true); //Ativa a captura das teclas para a janela.
	box(sWin, 0, 0); //Desenha a caixa com base (y, x) da janela criada.
	box(staWin, 0, 0);
	mvwprintw(sWin, 0, 0, "Jogada: P1");
	mvwprintw(staWin, 0, 0, "Stack");

	wrefresh(sWin);
	wrefresh(staWin);

	while(esco != 'q') {

		wattron(sWin, A_REVERSE);
		//mvwprintw(sWin, 1, 2, "mov %c", movts[sel]);
		escSta(2, 1, movts[sel], sWin);
		wattroff(sWin, A_REVERSE);

		esco = wgetch(sWin);

		switch(esco){
			case KEY_UP:
				sel--;
				sel = (sel <= -1) ? 4 : sel;
				break;
			case KEY_DOWN:
				sel++;
				sel = (sel >= 5) ? 0 : sel;
				break;
		}
		//10 == valor da tecla ENTER.
		if(esco == 10 && nsel <= 7){
			nmovts[nsel] = movts[sel];
			escSta(1, nsel+1, movts[sel], staWin);
			nsel++;
		}
		else if(esco == 'x' || nsel >= 7){
			for(i = 0; i < nsel && !(*usu).pbang; i++){
				wattron(staWin, A_REVERSE);
				escSta(1, i+1, nmovts[i], staWin);
				wattroff(staWin, A_REVERSE);
				sta = (nmovts[i] == 'e') ? entrTcl(sta, usu), sta : nmovts[i];
				sleep(1);
			}
			break;
		}
		else if(esco == 'q'){
			break;
		}
	}
	//mvwprintw(sWin, 1, 1, "Qualquer tecla para passar");
	//wrefresh(sWin);
	//getch();
	return 0;
}


void entrTcl(uint8_t input, Rbot * usu){
	int16_t nX, nY;
	switch(input){
		case 'u':
			nX = (*usu).xpos;
			nY = (*usu).ypos -1;
			//moverBot((*usu).ypos - 1, (*usu).xpos, usu);
			break;
		case 'l':
			nX = (*usu).xpos -1;
			nY = (*usu).ypos;
			//moverBot((*usu).ypos, (*usu).xpos - 1, usu);
			break;
		case 'd':
			nX = (*usu).xpos;
			nY = (*usu).ypos +1;
			//moverBot((*usu).ypos + 1, (*usu).xpos, usu);
			break;
		case 'r':
			nX = (*usu).xpos + 1;
			nY = (*usu).ypos;
			//moverBot((*usu).ypos, (*usu).xpos + 1, usu);
			break;
		default:
			break;
	}
	chkMov(nX, nY, usu);
}


void moverBot(uint8_t y, uint8_t x, Rbot * usu){
	mvprintw((*usu).ypos, (*usu).xpos, " ");
	(*usu).xpos = x;
	(*usu).ypos = y;
	mvprintw((*usu).ypos, (*usu).xpos, "@");
	move((*usu).ypos, (*usu).xpos);
	//delay(500);
	refresh();
	//sleep(1);
}


int16_t opJogada(Rbot * opo, int8_t mv){
	char *movs[6];
	movs[0] = "rrrr";
	movs[1] = "rrrr";
	movs[2] = "rrrr";
	movs[3] = "rrrr";
	movs[4] = "rrrd";
	movs[5] = "dddd";
	//movs[1][5] = ['r', 'r', 'r', 'r'];
	//movs[2][5] = ['r', 'r', 'r', 'r'];
	//movs[3][5] = "rrrd";
	//movs[3][5] = ['r', 'r', 'r', 'd'];
	int8_t i = 0;

	wclear(sWin);
	wclear(staWin);
	wrefresh(sWin);
	wrefresh(staWin);

	for (i = 0; i < 4; i++){
		entrTcl(movs[mv][i], opo);
		sleep(1);
	}
	return 0;
}
