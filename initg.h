#ifndef INITG_H_
#define INITG_H_
#include<ncurses.h>
#include<stdint.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>

int8_t finaliz(int8_t cond);

/* ###########--initg.c */
typedef struct {
	uint16_t xPos, yPos, altu, larg;
} Sala;

typedef struct {
	uint16_t xpos, ypos;
	bool pbang;
} Rbot;

WINDOW * sWin; //Define ponteiro usado para a janela de escolha do jogador.
WINDOW * staWin; //Define ponteiro usado para a janela da stack de movs.
WINDOW * bmbWin;
Rbot * botSetUp(int inix, int iniy);
uint16_t screenSetUp(int8_t dif); //define prototipo da função.


/* ###########--mvbot.c */
int16_t pJogada(Rbot * usu);
int16_t opJogada(Rbot * opo, int8_t mv);
void entrTcl(uint8_t input, Rbot * usu);
void moverBot(uint8_t y, uint8_t x, Rbot * usu);


/* ###########--bmbot.c */
int8_t ptBomb();
#endif //INITG_H_
