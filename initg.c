#include "initg.h"


uint16_t screenSetUp(int8_t dif) {
	int16_t xMax, yMax;

	initscr();	//Inicializa a tela do curses.
	noecho();	//Impede o usuário de digitar nesta tela.
	getmaxyx(stdscr, yMax, xMax); //Retorna "tamanho" da janela
	printw("j:jogada - q:sair - x:executar stack", yMax, 1);
	//			 (Altura, Largura, inicioY, inicioX)
	sWin = newwin(3, 30, yMax-3, 1);
	staWin = newwin(10, 15, yMax-10, 32);
	bmbWin = newwin(13, 40, yMax-13, 3);

	mvprintw(9, 0,  "___________________| ______| |");
	mvprintw(10, 0, "______| |_________|  __ !__| |");
	mvprintw(10, 0, "______| |________|   |_ _ _| |");
	mvprintw(11, 0, "_____|      |___|  |    ___| |");
	mvprintw(12, 0, "____|       |__|  |________|  |");
	mvprintw(13, 0, "___|  |   __     __________| |");
	mvprintw(14, 0, "___|                         |");
	mvprintw(15, 0, "___|  |____________________|");
	mvprintw(9, 23, "!");
	//mvprintw(4, 5, "!");
	//mvprintw(15, 5, "!");
	refresh();	//Atualiza os dados carregados na tela.
	return 0;

}

Rbot * botSetUp(int inix, int iniy){
	Rbot * newBot;
	newBot = malloc(sizeof(Rbot));

	(*newBot).xpos = inix;
	(*newBot).ypos = iniy;
	(*newBot).pbang = false;

	//printa o bot do player
	mvprintw((*newBot).ypos, (*newBot).xpos, "@");
	//move cursor para o slot do bot
	move((*newBot).ypos, (*newBot).xpos);

	return newBot;
}
