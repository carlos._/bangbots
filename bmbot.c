#include"initg.h"


int8_t ptBomb(){
	int8_t resp, entr;
	wclear(sWin);
	wclear(staWin);
	wrefresh(sWin);
	wrefresh(staWin);


	box(bmbWin, 0, 0); //Desenha a caixa com base (y, x) da janela criada.
	mvwprintw(bmbWin, 0, 0, "Desafio:");
	mvwprintw(bmbWin, 1, 2, "Test");
	//wrefresh(bmbWin);
	//entr = mvscanw(bmbWin,9,1,entr);
	mvwprintw(bmbWin, 1, 1, "int main(){");
	mvwprintw(bmbWin, 2, 4, "int j = 4, i = 0");
	mvwprintw(bmbWin, 3, 4, "i = i++, i+j, i");
	mvwprintw(bmbWin, 4, 4, "printf(\"%d\", i)");
	mvwprintw(bmbWin, 5, 1, "}");
	mvwprintw(bmbWin, 8, 1, "A saída do programa acima é ");
	mvwprintw(bmbWin, 9, 4, "(teclas) - a:1, b:3, c:0, d:NDA ??");
	mvwprintw(bmbWin, 11, 1, " ");
	entr = wgetch(bmbWin);
	resp = (entr == 'c') ? 1 : 0;
	//wgetch(bmbWin);
	wclear(bmbWin);
	wrefresh(bmbWin);

	return resp;
}
