CC=gcc -l ncurses

all: clean main

main:
	$(CC) main.c mvbot.c initg.c bmbot.c -o bangbots

clean:
	rm -f bangbots
